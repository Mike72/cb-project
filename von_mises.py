import numpy as np
from scipy import interpolate
from scipy.stats import vonmises
from assets import PARAMETERS


def inverse_transform(pdf, min_lim=-np.pi, max_lim=np.pi, density=1000):
    samples = np.linspace(min_lim, max_lim, density)
    discrete_pdf = pdf(samples)  # discrete pdf
    cdf = np.cumsum(discrete_pdf)  # cdf
    cdf = cdf / cdf.max()  # normalizes cdf
    inverse_cdf = interpolate.interp1d(cdf, samples)
    return inverse_cdf


def base_von_mises(base_orient, near_wall, wall_orient):
    if near_wall:
        def base_pdf(th):
            return (vonmises.pdf(th - wall_orient[0], PARAMETERS.Kw) +
                    vonmises.pdf(th - wall_orient[1], PARAMETERS.Kw))/2.
    else:
        def base_pdf(th):
            return vonmises.pdf(th - base_orient, PARAMETERS.K0)
    return base_pdf


def wavg_von_mises(weights, directions, kappa):
    if len(weights) != len(directions):
        print("Error: not enough weights given for all directions.")

    def wavg_pdf(th):
        wavg = 0.
        wsum = 0.
        for weight, direction in zip(weights, directions):
            wavg += weight * vonmises.pdf(th - direction, kappa)
            wsum += weight
        return wavg/wsum
    return wavg_pdf


def global_von_mises(base_orient, fish_sa, fish_dir, objects_sa, objects_dir, near_wall, wall_dir=None):
    base_pdf = base_von_mises(base_orient, near_wall, wall_dir)
    fish_pdf = None
    if len(fish_sa) > 0:
        fish_pdf = wavg_von_mises(fish_sa, fish_dir, PARAMETERS.Kf)
    objects_pdf = None
    if len(objects_sa) > 0:
        objects_pdf = wavg_von_mises(objects_sa, objects_dir, PARAMETERS.Ks)

    fish_sa_sum = sum(fish_sa)
    objects_sa_sum = sum(objects_sa)
    if near_wall:
        _alpha = PARAMETERS.alpha_w
        _beta = PARAMETERS.beta_w
    else:
        _alpha = PARAMETERS.alpha_0
        _beta = PARAMETERS.beta_0

    if fish_pdf is None and objects_pdf is None:
        global_pdf = base_pdf
    elif fish_pdf is None:
        def global_pdf(th):
            return (base_pdf(th) + _beta*objects_sa_sum * objects_pdf(th))/(1 + _beta*objects_sa_sum)
    elif objects_pdf is None:
        def global_pdf(th):
            return (base_pdf(th) + _alpha*fish_sa_sum * fish_pdf(th))/(1 + _alpha*fish_sa_sum)
    else:
        def global_pdf(th):
            return (base_pdf(th) + PARAMETERS.Wf*_alpha*fish_sa_sum * fish_pdf(th) +
                    PARAMETERS.Ws*_beta*objects_sa_sum * objects_pdf(th)) / \
                   (1 + PARAMETERS.Wf*_alpha*fish_sa_sum + PARAMETERS.Ws*_beta*objects_sa_sum)

    return global_pdf


if __name__ == "__main__":
    pass
