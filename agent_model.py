import random
import math
import numpy as np
from assets import PARAMETERS, draw_speed_random, draw_speed_coupled
from solid_angles import normalize_angle, normalize_angles, \
    compute_fish_projections, compute_object_projections, compute_sa
from von_mises import global_von_mises, inverse_transform


class FishAgent:
    def __init__(self, identifier, dimensions, fov, init_pos, init_orientation,
                 avoidance_behaviour=False, coupled_linear_speed=False):
        """
        Constructs a fish agent with following parameters:
        :param identifier: identifier of agent for tracking purposes
        :param dimensions: dimensions, given as [length, width, height]
        :param fov: field of vision in 2D, given as [min_rad, max_rad]
        :param init_pos: initial position of head, given as [x, y, z]
        :param init_orientation: initial orientation of fish, given in rad
        :param avoidance_behaviour: if True, will simulate avoidance behaviour
        :param coupled_linear_speed: if True, will compute linear speed based on angular speed
        """
        self.identifier = identifier
        self.length = dimensions[0]
        self.width = dimensions[1]
        self.height = dimensions[2]
        self.fov = fov
        self.position = init_pos
        self.linear_speed = 0.
        self.angular_speed = 0.
        self.orientation = init_orientation
        self.nextOrientation = init_orientation
        self.avoidance = avoidance_behaviour
        self.coupled_speed = coupled_linear_speed
        self.hitWall = [False, False, False, False]  # right wall, top wall, left wall, bottom wall in that order

    def get_identifier(self):
        return self.identifier

    def get_head_position(self):
        return self.position

    def get_orientation(self):
        return self.orientation

    def get_linear_speed(self):
        return self.linear_speed

    def get_angular_speed(self):
        return self.angular_speed

    def get_center_position(self):
        center = [-self.length/3., 0.]
        rotation_matrix = [[math.cos(self.orientation), -math.sin(self.orientation)],
                           [math.sin(self.orientation), math.cos(self.orientation)]]
        rotated_center = np.matmul(rotation_matrix, center)
        rotated_center[0] += self.position[0]
        rotated_center[1] += self.position[1]
        rotated_center = np.append(rotated_center, self.position[2])
        return rotated_center

    # Given as [head_position, tail_position, left_fin, right_fin, upper_side, lower_side]
    def get_body_position(self):
        base_vertices = [[0., 0.], [-self.length, 0.],
                         [-self.length/3., self.width/2.], [-self.length/3., -self.width/2.],
                         [-self.length/3., 0.], [-self.length/3., 0.]]
        body_vertices = []
        rotation_matrix = [[math.cos(self.orientation), -math.sin(self.orientation)],
                           [math.sin(self.orientation), math.cos(self.orientation)]]
        for index, vertex in enumerate(base_vertices):
            rotated_vertex = np.matmul(rotation_matrix, vertex)
            if index < 4:
                body_vertices.append([rotated_vertex[0], rotated_vertex[1], 0.])
            elif index == 4:
                body_vertices.append([rotated_vertex[0], rotated_vertex[1], self.height/2.])
            elif index == 5:
                body_vertices.append([rotated_vertex[0], rotated_vertex[1], -self.height/2.])

        for vertex in body_vertices:
            vertex[0] += self.position[0]
            vertex[1] += self.position[1]
            vertex[2] += self.position[2]
        return body_vertices

    def is_under_disc(self, discs):
        is_under_disc = False
        for disc in discs:
            center, radius = disc.get_position()
            distance = math.sqrt((center[0] - self.position[0])**2 + (center[1] - self.position[1])**2)
            if distance < radius:
                is_under_disc = True
                break
        return is_under_disc

    def get_integration_limits(self):
        if self.hitWall[0]:  # hit the right wall
            if self.hitWall[1]:  # in top right corner
                return -np.pi, -np.pi/2
            elif self.hitWall[3]:  # in bottom right corner
                return np.pi/2, np.pi
            else:
                return np.pi/2, 3*np.pi/2  # god I hope this one works
        elif self.hitWall[2]:  # hit the left wall
            if self.hitWall[1]:  # in top left corner
                return -np.pi/2, 0.
            elif self.hitWall[3]:  # in bottom left corner
                return 0., np.pi/2
            else:
                return -np.pi/2., np.pi/2
        elif self.hitWall[1]:  # hit the top wall
            return -np.pi, 0.
        elif self.hitWall[3]:  # hit the bottom wall
            return 0., np.pi
        else:
            return -np.pi, np.pi

    def compute_orientation(self, fish_agents, objects_interest, environment_dimensions, Dt=1./3.):
        # Dt is needed only if coupled speed computation is used
        head_position = self.get_head_position()

        near_wall = False
        near_corner = False
        wall_dir = [self.orientation, self.orientation]

        if head_position[0] < PARAMETERS.Dw:  # we are near vertical wall on y axis
            near_wall = True
            if head_position[1] < PARAMETERS.Dw:  # we are in bottom left corner
                near_corner = True
                wall_dir = [PARAMETERS.RIGHT, PARAMETERS.UP]
            elif environment_dimensions[1] - head_position[1] < PARAMETERS.Dw:  # we are in top left corner
                near_corner = True
                wall_dir = [PARAMETERS.RIGHT, PARAMETERS.DOWN]
            else:
                wall_dir = [PARAMETERS.UP, PARAMETERS.DOWN]
        elif environment_dimensions[0] - head_position[0] < PARAMETERS.Dw:  # we are near other vertical wall
            near_wall = True
            if head_position[1] < PARAMETERS.Dw:  # we are in bottom right corner
                near_corner = True
                wall_dir = [PARAMETERS.LEFT, PARAMETERS.UP]
            elif environment_dimensions[1] - head_position[1] < PARAMETERS.Dw:  # we are in top right corner
                near_corner = True
                wall_dir = [PARAMETERS.LEFT, PARAMETERS.DOWN]
            else:
                wall_dir = [PARAMETERS.UP, PARAMETERS.DOWN]
        elif head_position[1] < PARAMETERS.Dw or environment_dimensions[1] - head_position[1] < PARAMETERS.Dw:
            # we are near a horizontal wall and not in a corner
            near_wall = True
            wall_dir = [PARAMETERS.RIGHT, PARAMETERS.LEFT]

        if near_corner:  # this prevents circling in corners (we only ever take closest direction)
            difference_0 = abs(self.orientation - wall_dir[0]) % (2 * np.pi)
            difference_1 = abs(self.orientation - wall_dir[1]) % (2 * np.pi)
            if min(difference_0, 2*np.pi - difference_0) < min(difference_1, 2*np.pi - difference_1):
                wall_dir = [wall_dir[0], wall_dir[0]]
            else:
                wall_dir = [wall_dir[1], wall_dir[1]]

        elif near_wall:  # this prevents flipping near walls (we ignore direction in blind spot)
            for index, direction in enumerate(wall_dir):
                difference = abs(self.orientation - direction) % (2*np.pi)
                if min(difference, 2*np.pi - difference) > np.pi*3./4.:
                    wall_dir[index] = wall_dir[1 - index]
                    break

        fov = [(self.fov[0] + self.orientation), (self.fov[1] + self.orientation)]
        fov = normalize_angles(fov)

        fish_spherical_polygons = []
        object_spherical_polygons = []

        for fish in fish_agents:
            fish_projection = compute_fish_projections(head_position, fish, self.avoidance)
            fish_spherical_polygons.append(fish_projection)
        for obj in objects_interest:
            obj_projection = compute_object_projections(head_position, obj)
            object_spherical_polygons.append(obj_projection)

        fish_sa, fish_dir, objects_sa, objects_dir = compute_sa(fov, fish_spherical_polygons, object_spherical_polygons)
        integration_limits = self.get_integration_limits()

        global_pdf = global_von_mises(self.orientation, fish_sa, fish_dir, objects_sa, objects_dir,
                                      near_wall, wall_dir)
        inverse_cdf = inverse_transform(global_pdf, min_lim=integration_limits[0], max_lim=integration_limits[1])
        # edges aren't included to prevent integration errors
        next_orientation = inverse_cdf(random.uniform(0.025, 0.975))

        self.nextOrientation = next_orientation
        if self.coupled_speed:
            orientation_change = self.nextOrientation - self.orientation
            self.angular_speed = normalize_angle(orientation_change)/Dt
            self.linear_speed = draw_speed_coupled(self.angular_speed)
            if self.is_under_disc(objects_interest):
                self.linear_speed *= PARAMETERS.xi_disc
        else:
            self.linear_speed = draw_speed_random(self.is_under_disc(objects_interest))

    def move(self, dt, environment_dimensions):
        if self.coupled_speed:
            # we are moving with some angular_speed and linear_speed along a circle arc with some radius
            angle_change = self.angular_speed*dt
            radius = self.linear_speed/self.angular_speed
            # the distance is equal to the circle chord between the points
            distance = 2 * radius * math.sin(angle_change/2.)
            direction = self.orientation + angle_change/2.

            rotation_matrix = [[math.cos(direction), -math.sin(direction)],
                               [math.sin(direction), math.cos(direction)]]
            translation_vector = np.matmul(rotation_matrix, [distance, 0.])

            self.position[0] += translation_vector[0]
            self.position[1] += translation_vector[1]
            self.orientation = self.orientation + angle_change

        else:
            speed = self.linear_speed
            direction = self.nextOrientation

            rotation_matrix = [[math.cos(direction), -math.sin(direction)],
                               [math.sin(direction), math.cos(direction)]]
            translation_vector = np.matmul(rotation_matrix, [speed, 0.]) * dt

            self.position[0] += translation_vector[0]
            self.position[1] += translation_vector[1]
            self.orientation = direction

        # we adjust position if agent moved out of the tank
        if self.position[0] < 0.:
            self.position[0] = 0.
            self.hitWall[0] = False  # right wall
            self.hitWall[2] = True  # left wall
        elif self.position[0] > environment_dimensions[0]:
            self.position[0] = environment_dimensions[0]
            self.hitWall[0] = True
            self.hitWall[2] = False
        else:
            self.hitWall[0] = False
            self.hitWall[2] = False

        if self.position[1] < 0.:
            self.position[1] = 0.
            self.hitWall[1] = False  # top wall
            self.hitWall[3] = True  # bottom wall
        elif self.position[1] > environment_dimensions[1]:
            self.position[1] = environment_dimensions[1]
            self.hitWall[1] = True
            self.hitWall[3] = False
        else:
            self.hitWall[1] = False
            self.hitWall[3] = False
