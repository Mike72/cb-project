import math
import numpy as np
from spherical_geometry import vector as sg_vector
from spherical_geometry import polygon as sg_polygon
from assets import PARAMETERS


"""
NOTE: some of these functions are not used during simulation, as solid angle
      computation was reimplemented with the spherical_geometry library
"""


# this normalizes angle onto [-pi, pi]
def normalize_angle(angle):
    while angle < - np.pi:
        angle += 2 * np.pi
    while angle > np.pi:
        angle -= 2 * np.pi
    return angle


# wrapper for normalizing list of angles
def normalize_angles(fov):
    for i in range(len(fov)):
        fov[i] = normalize_angle(fov[i])
    return fov


# converts cartesian coordinates into [azimuth, latitude, distance]
def cartesian_to_spherical(x, y, z):
    distance = np.linalg.norm([x, y, z])
    return [math.atan2(y, x), math.asin(z / distance), distance]


# converts spherical coordinates into [x, y, z]
def spherical_to_cartesian(azimuth, latitude, distance=1.):
    x = math.cos(azimuth) * math.cos(latitude) * distance
    y = math.sin(azimuth) * math.cos(latitude) * distance
    z = math.sin(latitude) * distance
    return [x, y, z]


def great_circle_distance(v1, v2):
    a = math.cos(v1[1])*math.cos(v2[1])*math.cos(v1[0] - v2[0])
    b = math.sin(v1[1])*math.sin(v2[1])
    return math.acos(a+b)


def triangle_spherical_excess(spherical_triangle):
    a = great_circle_distance(spherical_triangle[0], spherical_triangle[1])
    b = great_circle_distance(spherical_triangle[0], spherical_triangle[2])
    c = great_circle_distance(spherical_triangle[1], spherical_triangle[2])
    s = (a+b+c)/2.
    product = math.tan(s/2.)*math.tan((s-a)/2.)*math.tan((s-b)/2.)*math.tan((s-c)/2.)
    return 4 * math.atan(math.sqrt(product))


def compute_intersection(v1, v2, azimuth):
    # computes the latitude of great circle passing through v1 and v2 at given azimuth
    # v1 and v2 must be given as [azimuth, latitude]
    a = math.tan(v2[1]) * math.sin(v1[0] - azimuth)
    b = math.tan(v1[1]) * math.sin(azimuth - v2[0])
    c = math.sin(v1[0] - v2[0])
    latitude = math.atan((a + b)/c)
    return [azimuth, latitude]


def compute_unobstructed_spherical_excess(sph_triangle: sg_polygon.SphericalPolygon, obstructions):
    global_obstruction = sg_polygon.SphericalPolygon.multi_union(obstructions)
    spherical_excess = sph_triangle.area()
    obstructed_portion = sph_triangle.overlap(global_obstruction)
    return spherical_excess * (1. - obstructed_portion)


def compute_fish_projections(pov_loc, fish, avoidance_behaviour=False):
    """
    :param pov_loc: location of the perceptor (eye)
    :param fish: observed fish to project
    :param avoidance_behaviour: True if aligning behaviour should be simulated
    :return: [dist, sph_center, spherical_projection, direction], where
            dist is the distance from pov_loc to center of object
            sph_center is the center of the object in spherical coordinates
            spherical_projection is a SphericalPolygon
            direction is the direction of the vector from pov_loc to center of object
    """
    center = fish.get_center_position()
    center = [center[0]-pov_loc[0], center[1]-pov_loc[1], center[2]-pov_loc[2]]
    sph_center = cartesian_to_spherical(center[0], center[1], center[2])
    direction = sph_center[0]
    dist = sph_center[2]
    if avoidance_behaviour and dist < PARAMETERS.Da:
        direction = normalize_angle(2 * fish.get_orientation() - direction)

    fish_coords = fish.get_body_position()
    for vertex in fish_coords:
        vertex[0] -= pov_loc[0]
        vertex[1] -= pov_loc[1]
        vertex[2] -= pov_loc[2]

    # Because fish are swimming in the same plane, the top and bottom vertex (last 2) will always be visible.
    # The first four coords are of head, tail, left fin and right fin in that order.
    azimuths = []
    for ind in range(4):
        azimuth = cartesian_to_spherical(fish_coords[ind][0], fish_coords[ind][1], fish_coords[ind][2])[0]
        if azimuth < 0.:
            azimuth += 2*np.pi  # We make all azimuths positive
        azimuths.append(azimuth)

    if abs(azimuths[0] - azimuths[1]) > abs(azimuths[2] - azimuths[3]):
        v1, v2 = fish_coords[0], fish_coords[1]  # head-tail is larger angle
    else:
        v1, v2 = fish_coords[2], fish_coords[3]  # left_fin-right_fin is larger angle

    # next we normalize center to the unit sphere
    center = sg_vector.normalize_vector(center)

    # we construct the spherical polygon
    v1 = sg_vector.normalize_vector(v1)
    v2 = sg_vector.normalize_vector(v2)
    v3 = sg_vector.normalize_vector(fish_coords[4])
    v4 = sg_vector.normalize_vector(fish_coords[5])

    projection = sg_polygon.SphericalPolygon([v1, v3, v2, v4, v1], inside=center)

    return [dist, center, projection, direction]


def compute_object_projections(pov_loc, obj):
    """
    :param pov_loc: location of the perceptor (eye)
    :param obj: observed object to project
    :return: [dist, sph_center, spherical_projection, direction], where
            dist is the distance from pov_loc to center of object
            sph_center is the center of the object in spherical coordinates
            spherical_projection is a SphericalPolygon
            direction is the direction of the vector from pov_loc to center of object
    note: this function only approximates the disk by a polygon with given number of vertices.
    """
    center, radius = obj.get_position()
    center = [center[0]-pov_loc[0], center[1]-pov_loc[1], center[2]-pov_loc[2]]
    sph_center = cartesian_to_spherical(center[0], center[1], center[2])
    direction = sph_center[0]
    dist = sph_center[2]

    # we generate the vertices of triangles
    no_of_vertices = obj.get_no_of_vertices()
    boundary_vertices = []
    for ind in range(no_of_vertices):
        angle = ind * 2. * np.pi / no_of_vertices
        boundary_vertices.append([math.cos(angle) * radius, math.sin(angle) * radius, 0])
    for vertex in boundary_vertices:  # note that the pov_loc is already subtracted from center :')
        vertex[0] += center[0]
        vertex[1] += center[1]
        vertex[2] += center[2]

    # next we normalize center to the unit sphere
    center = sg_vector.normalize_vector(center)

    # we generate the spherical polygon
    vertices = []
    for vertex in boundary_vertices:
        v = sg_vector.normalize_vector(vertex)
        vertices.append(v)

    vertices.append(vertices[0])  # we close the projection (first and last vertex must be the same)
    projection = sg_polygon.SphericalPolygon(vertices, inside=center)

    return [dist, center, projection, direction]


def compute_sa(fov, fish_spherical_polygons, object_spherical_polygons):
    """
    :param fov: azimuthal field of vision given as [min, max] in radians
    :param fish_spherical_polygons: list returned by compute_fish_projections
    :param object_spherical_polygons: list returned by compute_object_projections
    :return: (fish_sa, fish_dir, objects_sa, objects_dir), where
            fish_sa is the list of (unobstructed) solid angles of observed fish
            fish_dir is the list of directions towards centers of observed fish, in same order as fish_sa
            objects_sa is the list of (unobstructed) solid angles of observed objects
            objects_dir is the list of directions towards centers of observed objects, in same order as objects_sa
    note: if center of fish or object cannot be seen, it is skipped
    note: if center of a triangle cannot be seen, it is discarded
    note: if all three vertices of a triangle cannot be seen, it is discarded
    note: if the spherical excess of a fish or object is too small, it is skipped
    """
    # we sort fish and objects based on distance while keeping them separate
    fish_sorted = sorted(fish_spherical_polygons, key=lambda k: k[0])
    objects_sorted = sorted(object_spherical_polygons, key=lambda k: k[0])

    blind_spot_azimuth = (fov[0] + fov[1]) / 2.  # this works if min > max on [-pi, pi]
    if fov[0] < fov[1]:
        blind_spot_azimuth += np.pi  # if min < max we rotate by pi
        if blind_spot_azimuth > np.pi:  # and ensure it is still on [-pi, pi]
            blind_spot_azimuth -= 2 * np.pi

    # we generate the blind spot as a spherical quadrilateral
    v1 = spherical_to_cartesian(blind_spot_azimuth, np.pi/2. - 0.01)  # (almost) north pole
    v2 = spherical_to_cartesian(fov[0], 0.)  # min of fov
    v3 = spherical_to_cartesian(blind_spot_azimuth, -np.pi/2. + 0.01)  # (almost) south pole
    v4 = spherical_to_cartesian(fov[1], 0.)  # max of fov

    blind_spot = spherical_to_cartesian(blind_spot_azimuth, 0.)
    blind_area = sg_polygon.SphericalPolygon([v1, v2, v3, v4, v1], inside=blind_spot)

    obstructions = [blind_area]
    fish_sa = []
    fish_dir = []
    objects_sa = []
    objects_dir = []
    i = 0  # counter for fish
    j = 0  # counter for objects
    while i < len(fish_sorted) or j < len(objects_sorted):
        if j >= len(objects_sorted):  # we have reached the end of objects
            current_is_fish = True
        elif i >= len(fish_sorted):  # we have reached the end of fish
            current_is_fish = False
        elif fish_sorted[i][0] < objects_sorted[j][0]:  # the next fish is closer
            current_is_fish = True
        else:  # the next object is closer
            current_is_fish = False

        if current_is_fish:
            currently_observed = fish_sorted[i]
            i += 1
        else:
            currently_observed = objects_sorted[j]
            j += 1

        projection = currently_observed[2]
        spherical_excess = compute_unobstructed_spherical_excess(projection, obstructions)
        obstructions.append(projection)
        if spherical_excess > 0.0001:
            if current_is_fish:
                fish_sa.append(spherical_excess)
                fish_dir.append(currently_observed[3])
            else:
                objects_sa.append(spherical_excess)
                objects_dir.append(currently_observed[3])

    return fish_sa, fish_dir, objects_sa, objects_dir


if __name__ == "__main__":
    pass
