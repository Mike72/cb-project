import random
import itertools
import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
from assets import ObjectOfInterest
from agent_model import FishAgent


class EnvironmentModel:
    def __init__(self, dimensions, fish_agents, objects_interest):
        self.dimensions = dimensions
        self.fishAgents = set(fish_agents)
        self.objectsOfInterest = set(objects_interest)

    def compute_orientation(self, Dt=1./3.):
        # Dt is needed only if coupled speed computation is used
        for fish_agent in self.fishAgents:
            fish_agent.compute_orientation(self.fishAgents.difference([fish_agent]),
                                           self.objectsOfInterest, self.dimensions, Dt)

    def move_agents(self, dt):
        for fish_agent in self.fishAgents:
            fish_agent.move(dt, self.dimensions)

    def get_agent_positions(self):
        agent_positions = []
        for fish_agent in self.fishAgents:
            fish_id = fish_agent.get_identifier()
            fish_coords = fish_agent.get_body_position()
            agent_positions.append((fish_id, fish_coords))
        return agent_positions

    def get_agent_orientations(self):
        agent_rotations = []
        for fish_agent in self.fishAgents:
            fish_id = fish_agent.get_identifier()
            fish_rotation = fish_agent.get_orientation()
            agent_rotations.append((fish_id, fish_rotation))
        return agent_rotations

    def get_agent_linear_speeds(self):
        agent_speeds = []
        for fish_agent in self.fishAgents:
            fish_id = fish_agent.get_identifier()
            fish_speed = fish_agent.get_linear_speed()
            agent_speeds.append((fish_id, fish_speed))
        return agent_speeds

    def get_agent_angular_speeds(self):
        agent_speeds = []
        for fish_agent in self.fishAgents:
            fish_id = fish_agent.get_identifier()
            fish_speed = fish_agent.get_angular_speed()
            agent_speeds.append((fish_id, fish_speed))
        return agent_speeds

    def get_interagent_distances(self):
        distances = []
        agents = list(self.fishAgents)
        for pair in itertools.combinations(agents, 2):
            fish_0 = np.array(pair[0].get_head_position())
            fish_1 = np.array(pair[1].get_head_position())
            distances.append(np.linalg.norm(fish_0 - fish_1))
        return distances


def append_coordinates(head_coordinates, body_coordinates, current_positions, current_rotations):
    body_pos = dict()

    for position in current_positions:
        fish_id = position[0]
        fish_coords = position[1]
        head_position = [fish_coords[0][0], fish_coords[0][1]]
        body_position = [(fish_coords[0][0] + fish_coords[1][0])/2., 0.25,
                         (fish_coords[0][1] + fish_coords[1][1])/2.]

        head_coordinates[fish_id].append(head_position)
        body_pos[fish_id] = body_position

    for rotation in current_rotations:
        fish_id = rotation[0]
        body_pos[fish_id].append(np.rad2deg(rotation[1]))

    for fish_id in body_pos:
        body_coordinates[fish_id].append(body_pos[fish_id])

    return head_coordinates, body_coordinates


def write_to_csv(coordinates):
    for fish_id in coordinates:
        with open('data/fish_' + str(fish_id) + '.csv', 'w+', newline='') as f:
            csv_writer = csv.writer(f, delimiter=',', quoting=csv.QUOTE_MINIMAL)
            for coords in coordinates[fish_id]:
                csv_writer.writerow(coords)


def draw_coordinates(coordinates):
    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
              'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    for index, fish_id in enumerate(coordinates):
        color = colors[index % 10]
        x = []
        y = []
        for coords in coordinates[fish_id]:
            x.append(coords[0])
            y.append(coords[1])
            if len(x) > 1.:
                plt.plot(x, y, color=color, alpha=0.6, solid_capstyle="butt")
                x = [x[1]]
                y = [y[1]]
    plt.gca().set_aspect('equal', adjustable='box')
    plt.gca().set_xlim(0.0, 1.2)
    plt.gca().set_ylim(0.0, 1.2)
    plt.show()
    plt.close()


def generate_random_fish_agent(ident, dims, fov, vertical_pos, environment_dims, avoidance, coupled_speed):
    coord_x = random.uniform(0, environment_dims[0])
    coord_y = random.uniform(0, environment_dims[1])
    orientation = random.uniform(-np.pi, np.pi)
    return FishAgent(ident, dims, fov, [coord_x, coord_y, vertical_pos], orientation,
                     avoidance_behaviour=avoidance, coupled_linear_speed=coupled_speed)


def simulate_model(no_of_fish, objects_present, total_seconds, computational_steps=3, movement_steps=1,
                   avoidance_behaviour=False, coupled_speed=False,
                   plot_paths=False, output_paths=False, return_distances=False, plot_distances=False):
    """
    :param no_of_fish: number of randomly generated fish agents to simulate
    :param objects_present: if True, simulates two discs of radius 0.1 at [0.25, 0.25, 0.3] and [0.95, 0.95, 0.3]
    :param total_seconds: total time to simulate in seconds
    :param computational_steps: number of time to compute orientation changes per second
    :param movement_steps: number of times to move fish agent per computational step (for smoother motion)
    :param avoidance_behaviour: if True, will simulate fish agents with avoidance behaviour
    :param coupled_speed: if True, will simulate fish agents with coupled linear speed computation
                          if False, will simulate fish agents with randomly drawn speeds
    :param plot_paths: if True, will plot the path of fish
    :param output_paths: if True, will output file fish_i for each fish with index i,
                         where each line represents one time step with format:
                         coord_x (left/right), coord_z (up/down), coord_y (forward/backward), rotation (in degrees)
                         all coords are for the center of the fish body
                         this is enough for basic playback in a rendering program like Unity
    :param return_distances: if True, will return a list of observed distances between fish agents during simulation
    :param plot_distances: if True, will plot the distribution of observed distances
    :return: either 'coordinates' or ('coordinates', 'distances') depending on value of return_distances
             'coordinates' are a dictionary with fish agent index as key, and a list of its 'coords' as value
             'coords' are ordered by time, each 'coords' list being of same format as a line described in output_paths

    """
    environment_dims = (1.2, 1.2, 0.3)
    fish_dims = (0.035, 0.01, 0.01)
    fish_fov = (-0.75 * np.pi, 0.75 * np.pi)
    fish_vertical_pos = 0.25

    objects_of_interest = []
    fish = []
    head_coordinates = dict()  # head coordinates are computed separately for smoother plots
    body_coordinates = dict()
    for i in range(no_of_fish):
        fish.append(generate_random_fish_agent(i, fish_dims, fish_fov, fish_vertical_pos, environment_dims,
                                               avoidance_behaviour, coupled_speed))
        head_coordinates[i] = []
        body_coordinates[i] = []

    if objects_present:
        objects_of_interest.append(ObjectOfInterest([0.25, 0.25, 0.3], radius=0.1))
        objects_of_interest.append(ObjectOfInterest([0.95, 0.95, 0.3], radius=0.1))

    model = EnvironmentModel(environment_dims, fish, objects_of_interest)
    time_delta = 1. / computational_steps  # this is for coupled speed computation
    time_diff = 1. / (computational_steps * movement_steps)
    total_time = 0.
    distances = []

    for i in range(total_seconds):
        for j in range(computational_steps):
            model.compute_orientation(time_delta)
            for k in range(movement_steps):
                total_time += time_diff
                model.move_agents(time_diff)
                head_coordinates, body_coordinates = append_coordinates(head_coordinates, body_coordinates,
                                                                        model.get_agent_positions(),
                                                                        model.get_agent_rotations())
                if return_distances or plot_distances:
                    distances += model.get_interagent_distances()

    if plot_paths:
        draw_coordinates(head_coordinates)
    if output_paths:
        write_to_csv(body_coordinates)

    if plot_distances and no_of_fish > 1:
        dist_bins = [0., 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8]
        plt.hist(np.array(distances), bins=dist_bins, density=True)
        plt.show()
        plt.close()

    if return_distances:
        return body_coordinates, distances
    else:
        return body_coordinates


def plot_heatmap(iterations, total_seconds, no_of_fish, objects_present,
                 computational_steps=3, avoidance=False, coupled_speed=False,
                 heatmap_cap=None, plot_distances=False):
    heatmap = np.zeros((30, 30))
    dist_bins = [0., 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8]

    distances = []
    for i in range(iterations):
        print("Iteration " + str(i+1))
        coordinates, dists = simulate_model(total_seconds=total_seconds, no_of_fish=no_of_fish,
                                            objects_present=objects_present, computational_steps=computational_steps,
                                            avoidance_behaviour=avoidance, coupled_speed=coupled_speed,
                                            return_distances=True)
        for fish_id in coordinates:
            for coords in coordinates[fish_id]:
                x_coord = min(int(coords[0] * 25), 29)
                y_coord = min(int(coords[2] * 25), 29)
                heatmap[x_coord][y_coord] += 1./(iterations*total_seconds*no_of_fish*computational_steps)
        distances += dists

    if heatmap_cap is not None:
        for i in range(30):
            for j in range(30):
                if heatmap[i][j] > heatmap_cap:
                    heatmap[i][j] = heatmap_cap

    fig, ax = plt.subplots()
    im = ax.imshow(heatmap, cmap='Oranges')
    cbar = ax.figure.colorbar(im, ax=ax)
    fig.tight_layout()
    plt.show()
    plt.close()

    if plot_distances:
        plt.hist(np.array(distances), bins=dist_bins, density=True)
        plt.show()
        plt.close()


if __name__ == "__main__":
    simulate_model(no_of_fish=2, objects_present=True, total_seconds=5*1, computational_steps=3,
                   movement_steps=1, avoidance_behaviour=True, coupled_speed=True,
                   plot_paths=False, output_paths=False, return_distances=True, plot_distances=False)
