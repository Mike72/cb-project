# Stochastic locomotion of zebrafish based on a visual sensory model

This repository contains an implementation of a stochastic vision-based
model for zebrafish constructed during the 2020/21 Collective Behaviour course at
the Faculty of Computer and Information Science, University of Ljubljana under the 
supervision of prof. dr. Iztok Lebar Bajec. Additional information
can be found in the project_report.pdf file. For any questions or inquiries I am available at mihaelrajh@gmail.com.


## Dependencies

**Version** indicates the version used during model implementation.
It is not a strict dependency.

| Library | Version |
|---|---|
| numpy | 1.19.0 |
| scipy | 1.5.4 |
| astropy | 4.2 |
| spherical-geometry | 1.2.19 |
| matplotlib | 3.3.3 |

## Usage

Only the parts of the code relevant to model simulation are documented here. Solid angle computations are
done in *solid_angles.py* and von Mises distributions are computed in *von_mises.py*.
Linear speed computation is done in *assets.py*. 

### global_model.py

This file contains the EnvironmentModel class, as well
as additional functions to quickly run simulations with default
parameters.

#### simulate_model(parameters)

Run a model simulation with specified parameters. It instantiates the 
model environment with dimensions (1.2m, 1.2m, 0.3m). This function can also
serve as a demo as to how to construct a custom simulation.

| parameter | description |
| --- | --- |
| no_of_fish (int) | Number of fish agents to simulate. They are generated randomly inside the environment bounds with size (3.5cm, 1cm, 1cm) at a vertical position of 0.25m and a field of vision of (-3pi/4, 3pi/4).
| objects_present (bool) |  If True, simulates two horizontal discs at (0.25m, 0.25m, 0.3m) and (0.95m, 0.95m, 0.3m) with a radius of 10cm.
| total_seconds (int) | The number of seconds for which to simulate the model.
| computational_steps (optional, default=3) | The number of times per second to recompute agent directions.
| movement_steps (optional, default=1) | The number of times per computation step to move the fish agents and save their coordinates (for smoother animation). 
| avoidance_behaviour (optional, default=False) | If True, simulates avoidance behaviour between fish agents.
| coupled_speed (optional, default=False) | If True, recomputes changes of direction as angular speed and draws linear speed from a joint distribution.
| plot_paths (optional, default=False) | If True, plots the resulting paths of fish agents.
| output_paths (optional, default=False) | If True, writes the coordinates of fish agents into a ./data/ folder. Each agent is assigned a file fish_i.csv, where i is the index of the agent (0 <= i < no_of_fish). Each line of the file is the recorded position of the agent center per movement step as *x_pos,z_pos,y_pos,dir*, where *z_pos* is the vertical position (constant) and *dir* is the agent heading in degrees.
| return_distances (optional, default=False) | If True, returns distances between fish agents as well as coordinates.
| plot_distances (optional, default=False) | If True, plots the distances between fish agents as a histogram.

**Return value:** coordinates or (coordinates, distances) depending on the value of *return_distances*.

The coordinates are a dictionary with agent indices as keys, and a list of their coordinates as value.
The list contains recorded coordinates at each movement step in the same format as output files (see *output_paths*).

The distances are a list of distances measured between each pair of agents at each movement step.

---

#### plot_heatmap(parameters)

Plot a heatmap of agent locations across specified number of simulations
using the *simulate_model* function.

| parameter | description |
| --- | --- |
| iterations (int) |  Number of times to simulate the model and record agent coordinates.
| total_seconds (int) | Total number of seconds to simulate the model per iteration.
| no_of_fish (int) | Number of fish agents to simulate. See *simulate_model* for details.
| objects_present (bool) | If True, simulates two horizontal discs. See *simulate_model* for details.
| computational_steps (optional, default=3) | The number of times per second to recompute agent directions.
| avoidance (optional, default=False) | If True, simulates avoidance behaviour between fish agents.
| coupled_speed (optional, default=False) | If True, recomputes changes of direction as angular speed and draws linear speed from a joint distribution.
| heatmap_cap (optional, default=None) | If not None, caps the heatmap values for each cell at specified value (a value of 0.003 is used in the report). This is done to prevent losing detail in areas with lower probability of presence.
| plot_distances (optional, default=False) | If True, plots the distances between fish agents across all iterations as a histogram.

**Return value:** None

---

#### class EnvironmentModel

Instantiate a new global simulation model.

| parameter | description |
| --- | --- |
| dimensions (float, float, float) | Defines the dimensions of the model as (x_dim, y_dim, z_dim) in m, where *z_dim* is the height and is ignored. Fish agents are constrained within [0. x_dim] on the x-axis and within [0., y_dim] on the y-axis.
| fish_agents (iterable of FishAgent) | Fish agents to simulate.
| objects_interest (iterable of ObjectOfInterest) | Discs to simulate.

| method | description |
| --- | --- |
| compute_orientation(Dt=1./3) | Recomputes the orientation and linear speed of all fish agents. The optional parameter Dt is only needed for angular speed computation when simulating using coupled speed. It specifies the time interval in seconds between the current and next call of the function.
| move_agents(dt) | Moves the agents according to current speed and heading for amount of seconds equal to dt.
| get_agent_positions() | Returns the current positions of every fish agent in the model. It is returned as a list of tuples, each tuple being (id, coords), where id is the identifier of the agent, and coords is the list returned by FishAgent.get_body_position()
| get_agent_orientations() | Returns the current directions of every fish agent in the model. It is returned as a list of tuples, each tuple being (id, dir), where id is the identifier of the agent, and dir is the value returned by FishAgent.get_orientation()
| get_agent_linear_speeds() | Returns the current linear speeds of every fish agent in the model. It is returned as a list of tuples, each tuple being (id, lin_speed), where iid the identifier of the agent, and lin_speed is its linear speed (in m/s).
| get_agent_angular_speeds() | Returns the current angular speeds of every fish agent in the model. It is returned as a list of tuples, each tuple being (id, ang_speed), where id the identifier of the agent, and ang_speed is its angular speed (in rad/s).
| get_interagent_distances() | Returns a list of current distances between all pairs of fish agents in the model.

### agent_model.py

#### class FishAgent

Instantiate a new fish agent.

| parameter | description |
| --- | --- |
| identifier | The id given to the agent for tracking purposes.
| dimensions (float, float, float) | The size of the agent given as (length, width, height) in m.
| fov (float, float) | The field of vision of the agent given as (fov_min, fov_max) in radians. The field of vision then goes from fov_min to fov_max in the positive direction depending on the agent's orientation.
| init_pos (float, float, float) | The initial position of the agent head given as (x_pos,y_pos,z_pos) in m, where z_pos is the vertical position.
| init_orientation (float) | The initial orientation of the agent given inside the xy-plane in radians. The x-axis corresponds to orientation of 0.
| avoidance_behaviour (optional, default=False) | If True, simulates avoidance behaviour between this fish agent and its conspecifics.
| coupled_linear_speed (optional, default=False) | If True, recomputes changes of direction as angular speed and draws linear speed from a joint distribution.

| method | description |
| --- | --- |
| get_identifier() | Returns the identifier of the agent.
| get_head_position() | Returns the current position [x, y, z] of the agent head.
| get_orientation() | Returns the current orientation of the agent in rad inside the xy-plane, with orientation 0 representing the positive direction of the x-axis.
| get_linear_speed() | Returns the current linear speed of the agent in m/s.
| get_angular_speed() | Returns the current angular speed of the agent in rad/s if using coupled speed computation, and 0 otherwise.
| get_center_position() | Returns the current position [x, y, z] of the agent center.
| get_body_position() | Returns the current positions of the agent vertices. It returns a list of six coordinate lists [x, y, z], each representing a vertex in the following order: head, tail, left fin, right fin, top vertex, bottom vertex.
| compute_orientation(fish_agents, objects_interest, environment_dimensions, Dt=1/3.) | Recomputes the orientation and linear speed of the agent based on the parameters. Conspecifics are given in fish_agents as an iterable of FishAgent, discs are given in objects_interest as an iterable of ObjectOfInterest, and environment_dimensions is the size of the environment given as [x, y, z] in m. The optional parameter Dt is only needed for angular speed computation when simulating using coupled speed. It specifies the time interval in seconds between the current and next call of the function.
| move(dt, environment_dimensions) | Moves the agent according to current speed and heading for amount of seconds equal to dt. The parameter environment_dimensions is the size of the environment given as [x, y, z] in m.

### assets.py

#### class ObjectOfInterest

Instantiate a new object of interest (horizontal disc).

| parameter | description |
| --- | --- |
| position (float, float, float) | The position of the disc given as (x_pos,y_pos,z_pos) in m, where z_pos is the vertical position.
| radius (float) | The radius of the discs (in m).
| no_of_vertices (optional, default=9) | During solid angle computation, the disc is approximated as a polygon with this number of vertices.

#### class Parameters

This object is considered static and holds most global model parameters.
Changing these values will change the agent behaviour.
