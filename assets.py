import math
import random
import numpy as np
from scipy import interpolate


class PARAMETERS:
    # orientation change parameters (in m)
    Dw = 0.05
    K0 = 6.3
    Kw = 20
    Kf = 20
    Ks = 20
    alpha_0 = 55
    alpha_w = 20
    beta_0 = 0.15
    beta_w = 0.01
    Wf = 0.5
    Ws = 1./9.
    # direction parameters
    RIGHT = 0.
    UP = np.pi/2
    LEFT = np.pi
    DOWN = -np.pi/2
    # linear speed parameters
    sigma_0 = 12.
    mi_u = 14.02  # in cm
    theta_u = 0.59
    sigma_u = 4.21  # in cm
    mi_omega = 0.  # in the source this is -0.02, a negligible amount
    theta_omega = 2.74
    sigma_omega = 2.85
    # additional parameter
    Da = 0.05  # avoidance distance
    xi_disc = 0.33  # how much to slow down when under disc


class ObjectOfInterest:
    def __init__(self, position, radius, no_of_vertices=9):
        """
        :param position: position of disc center in (x, y, z) coordinates
        :param radius: radius of disc
        :param no_of_vertices: during computation of spherical excess, the disc
                               is approximated as a polygon with a set number of vertices
        """
        self.position = position
        self.radius = radius
        self.n_vertices = no_of_vertices

    def get_position(self):
        return self.position, self.radius

    def get_no_of_vertices(self):
        return self.n_vertices


def draw_speed_random(is_under_disc):
    # this is random and only slightly matches the graphs from the articles
    # it doesn't match the observed distributions since they weren't downloaded and processed
    if is_under_disc:
        speed = np.random.normal(0.03, 0.02)
        if speed > 0.15:
            speed = 0.15
    else:
        speed = np.random.normal(0.09, 0.03)
        if speed > 0.25:
            speed = 0.25
    if speed < 0.005:
        speed = 0.005
    return speed


def coupling_function(linear_speed):
    return PARAMETERS.sigma_0 * ((2*PARAMETERS.sigma_0/PARAMETERS.sigma_omega) ** (-linear_speed/PARAMETERS.mi_u))


def joint_speed_probability(linear_speed, angular_speed):
    fc = coupling_function(linear_speed)
    factor1 = math.sqrt(PARAMETERS.theta_u * PARAMETERS.theta_omega)/(np.pi * PARAMETERS.sigma_u * fc)
    factor2 = - PARAMETERS.theta_u*((linear_speed - PARAMETERS.mi_u)/PARAMETERS.sigma_u)**2 \
              - PARAMETERS.theta_omega*((angular_speed - PARAMETERS.mi_omega)/fc)**2
    return factor1 * np.e**factor2


def draw_speed_coupled(angular_speed, min_lim=0.05, max_lim=30., density=1000):
    # SPEED IS COMPUTED IN CM!
    samples = np.linspace(min_lim, max_lim, density)
    discrete_pdf = []
    for sample in samples:
        discrete_pdf.append(joint_speed_probability(sample, angular_speed))
    cdf = np.cumsum(discrete_pdf)
    cdf = cdf / cdf.max()
    inverse_cdf = interpolate.interp1d(cdf, samples)
    return inverse_cdf(random.uniform(0.025, 0.975))/100.  # we divide by 100 to transform into m/s


if __name__ == "__main__":
    pass
